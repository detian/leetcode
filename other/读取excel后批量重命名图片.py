"""
读取目标 excel 后，根据其中的数据  对 图片批量改名

pip install pandas openpyxl


EXCEL_FILE_PATH：Excel文件的路径。
IMAGE_FOLDER：存放原始图片的文件夹路径。
NEW_FOLDER：存放重命名后图片的新文件夹路径。
IMAGE_EXTENSION：图片文件的扩展名。

"""
import os
import shutil
import pandas as pd

# 定义常量
EXCEL_FILE_PATH = './器具/家具/家具对应.xlsx'
IMAGE_FOLDER = './器具/家具/'
NEW_FOLDER = './new/器具/家具/'
IMAGE_EXTENSION = '.png'


COLUMN_NAME = 'ID'
COLUMN_VALUE_NAME = '名称'

# 读取Excel文件
df = pd.read_excel(EXCEL_FILE_PATH)

# 显示数据
# print(df)

# 将DataFrame转换为字典数组
equipment_dict = df.to_dict(orient='records')

# 创建一个新的文件夹来存放重命名后的文件
if not os.path.exists(NEW_FOLDER):
    os.makedirs(NEW_FOLDER)

# 遍历目标文件夹中的所有png文件
for filename in os.listdir(IMAGE_FOLDER):
    if filename.endswith(IMAGE_EXTENSION):
        # 移除文件扩展名
        # base_filename = filename[:-len(IMAGE_EXTENSION)]
        base_filename = filename[:-len(IMAGE_EXTENSION)]
        # 分割文件名以获取ID
        parts = base_filename.split('_')




        try:
            # 尝试将ID转换为整数
            item_id = int(parts[-1])
            # 遍历字典查找匹配的装备名
            for item in equipment_dict:
                if item[COLUMN_NAME] == item_id:
                    # 如果找到匹配，重命名文件
                    new_filename = f"{item[COLUMN_VALUE_NAME]}{IMAGE_EXTENSION}"
                    source_path = os.path.join(IMAGE_FOLDER, filename)
                    destination_path = os.path.join(NEW_FOLDER, new_filename)
                    shutil.copy(source_path, destination_path)
                    print(f"Copied and renamed {filename} to {new_filename}")
                    break
        except ValueError:
            # 如果ID不是整数，忽略此文件
            continue