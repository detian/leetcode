from PIL import Image


def transfer(infile, outfile):
    sizes = [
        {
            'width': 480,
            'height': 762
        },
        {
            'width': 720,
            'height': 1242
        },
        {
            'width': 144,
            'height': 192
        },
        {
            'width': 1080,
            'height': 1882
        },
        {
            'width': 1242,
            'height': 2688
        },
        {
            'width': 2688,
            'height': 1242
        },
        {
            'width': 828,
            'height': 1792
        },
        {
            'width': 1792,
            'height': 828
        },
        {
            'width': 1125,
            'height': 2436
        },
        {
            'width': 2436,
            'height': 1125
        },
        {
            'width': 1242,
            'height': 2208
        },
        {
            'width': 2208,
            'height': 1242
        },
        {
            'width': 750,
            'height': 1334
        },
        {
            'width': 1334,
            'height': 750
        },
        {
            'width': 640,
            'height': 1136
        }, {
            'width': 1136,
            'height': 640
        },
        {
            'width': 640,
            'height': 960
        },
        {
            'width': 2048,
            'height': 2732
        },
        {
            'width': 2732,
            'height': 2048
        },
        {
            'width': 1668,
            'height': 2388
        },
        {
            'width': 2388,
            'height': 1668
        },
        {
            'width': 1668,
            'height': 2224
        },
        {
            'width': 2224,
            'height': 1668
        },
        {
            'width': 1536,
            'height': 2048
        },
        {
            'width': 2048,
            'height': 1536
        },
        {
            'width': 768,
            'height': 1024
        },
        {
            'width': 1024,
            'height': 768
        },
    ]
    cunzhao_sizes = [
        {
            'width': 295,
            'height': 413
        },

    ]

    im = Image.open(infile)
    for size in cunzhao_sizes:
        reim = im.resize((size['width'], size['height']))  # 宽*高
        reim.save(outfile + str(size['width']) + "x" + str(size['height']) + ".png",
                  dpi=(10.0, 10.0))  ##200.0,200.0分别为想要设定的dpi值

if __name__ == '__main__':
    infile = r"./unknow.png"
    outfile = r""
    transfer(infile, outfile)

# -----------------------------------
# python 修改图像大小和分辨率
# https://blog.51cto.com/u_15077556/4328870
