"""
通过行复制文件行更新文件创建时间

pip install pandas openpyxl


"""
from datetime import datetime

TARGET_FOLDER = 'D:\\workspace\\模板\\vue+flask'
NEW_FOLDER = 'D:\\workspace\\外包\\myshop\\'

TARGET_FOLDER = 'D:\\workspace\\模板\\vue+flask'
NEW_FOLDER = 'D:\\workspace\\外包\\myshop'

import os

TARGET_FOLDER = 'D:\\workspace\\模板\\vue+flask'
NEW_FOLDER = 'D:\\workspace\\外包\\myshop'


def recursive_line_copy(src, dst):
    """
    递归地逐行复制文件夹及其文件内容
    """
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            recursive_line_copy(s, d)
        else:
            # 尝试使用不同的编码读取文件

            try:
                with open(s, 'r', encoding='utf-8') as src_file:
                    with open(d, 'w', encoding='utf-8') as dst_file:
                        for line in src_file:
                            dst_file.write(line)
            except UnicodeDecodeError:
                try:
                    with open(s, 'r', encoding='ISO-8859-1') as src_file:
                        with open(d, 'w', encoding='ISO-8859-1') as dst_file:
                            for line in src_file:
                                dst_file.write(line)
                except Exception as e:
                    print(f"无法处理文件 {s}: {e}")
                    continue
            # 更新目标文件的创建时间
            creation_time = datetime.now()
            creation_time= creation_time.timestamp()
            os.utime(d, (creation_time, creation_time))
            print(f"逐行复制并更新了文件 {s} 到 {d}")


# 开始递归逐行复制
recursive_line_copy(TARGET_FOLDER, NEW_FOLDER)
