def reciprocal_sum(n):
    """

    :return:
    """
    number_sum = 0
    def calculate_number_sum(number):
        result = 0
        for i in range(number + 1 ):
            result += i

        return result

    for i in range(1,n + 1):
        number_sum += 1 / calculate_number_sum(i)
    return number_sum


if __name__ == '__main__':
    #  1 , 2 , 3

    print(reciprocal_sum(100))
