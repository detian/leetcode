def caipai():
    def fapai(card_sequence) -> list:
        temp_paizu = [[], [], []]
        for i in range(21):
            if i % 3 == 0:
                temp_paizu[0].append(card_sequence[i])
            elif i % 3 == 1:
                temp_paizu[1].append(card_sequence[i])
            else:
                temp_paizu[2].append(card_sequence[i])
        return temp_paizu

    def computation_sequence(target_clo, temp_paizu) -> list:

        # 返回新的序列
        if target_clo == 0:
            result = temp_paizu[1] + temp_paizu[0] + temp_paizu[2]

        elif target_clo == 2:
            result = temp_paizu[0] + temp_paizu[2] + temp_paizu[1]
        else:
            result = temp_paizu[0] + temp_paizu[1] + temp_paizu[2]
        return result

    def computation_target_card(possable_card):
        set0 = set(possable_card[0])
        set1 = set(possable_card[1])
        set2 = set(possable_card[2])

        common_elements = set0.intersection(set1, set2)

        return common_elements.pop()

    card_sequence = list(range(1, 22))
    possable_position = []
    paizu = fapai(card_sequence)
    print(paizu)

    user_input = int(input("指定选中的牌在哪列 ：")) - 1
    possable_position.append(paizu[user_input])
    card_sequence = computation_sequence(user_input, paizu)
    paizu = fapai(card_sequence)
    print(paizu)

    user_input = int(input("指定选中的牌在哪列 ：")) - 1
    possable_position.append(paizu[user_input])
    card_sequence = computation_sequence(user_input, paizu)
    paizu = fapai(card_sequence)
    print(paizu)

    user_input = int(input("指定选中的牌在哪列 ：")) - 1
    possable_position.append(paizu[user_input])
    card_sequence = computation_sequence(user_input, paizu)
    paizu = fapai(card_sequence)

    print("===========")
    print(card_sequence)
    print(paizu)

    target_card = computation_target_card(possable_position)

    print(target_card)


if __name__ == '__main__':
    #  1 , 2 , 3
    caipai()
