from typing import List


class Solution:
    def longestPalindrome(self, s: str) -> str:
        # 遍历字符串，向左向右做对称
        n = len(s)
        if n == 0:
            return ''
        # 定义返回结果
        res = s[0]

        for i in range(0, n - 1):
            # 如果是偶数回文串,字符串可能和左侧一个相等，或者和右侧一个相等(因为是单向的，所以只能是向右相等)
            # 偶数，右侧相等
            if s[i] == s[i + 1]:
                j = 0

                while i - j >= 0 and i + 1 + j < n and s[i - j] == s[i + 1 + j]:
                    j += 1
                if len(s[i - j + 1:i + 1 + j]) > len(res):
                    res = s[i - j + 1:i + 1 + j]

            # 如果是奇数字符串，字符串左边的和字符串右边的相等
            if s[i - 1] == s[i + 1]:
                j = 0
                while i - 1 - j >= 0 and i + 1 + j < n and s[i - 1 - j] == s[i + 1 + j]:
                    j += 1
                if len(s[i - j:i + 1 + j]) > len(res):
                    res = s[i - j:i + 1 + j]
        return res


if __name__ == '__main__':
    str = 'aba'
    solution = Solution()
    print(solution.longestPalindrome(str))
