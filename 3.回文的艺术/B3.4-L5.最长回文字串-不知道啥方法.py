from typing import List


class Solution:
    def longestPalindrome(self, s: str) -> str:
        # 遍历字符串，向左向右做对称
        n = len(s)
        if n == 0:
            return ''
        # 定义返回结果
        res = s[0]

        def extend(i: int, j: int, s: str) -> str:
            while i >= 0 and j < n and s[i] == s[j]:
                j += 1
                i -= 1
            return s[i + 1:j]

        for i in range(0, n - 1):
            e1 = extend(i, i, s)
            e2 = extend(i, i + 1, s)
            if max(len(e1), len(e2)) > len(res):
                res = e1 if len(e1) > len(e2) else e2
        return res


if __name__ == '__main__':
    str = 'abcda'
    solution = Solution()
    print(solution.longestPalindrome(str))
