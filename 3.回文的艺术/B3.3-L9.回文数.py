from typing import List


class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x < 0:
            return False
        elif x == 0:
            return True
        elif x % 10 == 0:
            return False

        # 反转数字
        copy = x
        res = 0
        while x > 0:
            res = res * 10 + x % 10
            x = x // 10
        return copy == res


if __name__ == '__main__':
    num = 123213
    solution = Solution()
    print(solution.isPalindrome(num))
