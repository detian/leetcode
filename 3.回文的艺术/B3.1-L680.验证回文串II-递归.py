from locale import str
from typing import List


class Solution:
    def validPalindrome(self, s: str) -> bool:
        n = len(s)
        # 双指针
        l = 0
        r = n - 1
        return self.isPalindrome(s, l, r, False)

    # 定义一个方法
    def isPalindrome(self, s: str, l: int, r: int, flag: bool) -> bool:
        while l < r:
            if s[l] != s[r]:
                if flag == False:
                    return self.isPalindrome(s, l + 1, r, True) or self.isPalindrome(s, l, r - 1, True)
                return False
            l += 1
            r -= 1
        return True


if __name__ == '__main__':
    str = 'asdfdsa'
    solution = Solution()
    print(solution.validPalindrome(str))
