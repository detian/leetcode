from locale import str
from typing import List


class Solution:
    def validPalindrome(self, s: str) -> bool:
        n = len(s)
        # 双指针
        l = 0
        r = n - 1

        # 定义一个方法
        def isPalindrome(s: str, i: int, n: int) -> bool:
            l = 0
            r = n - 1
            while l < r:
                if l == i:
                    l += 1
                elif r == i:
                    r -= 1
                if s[l] != s[r]:
                    return False
                l += 1
                r -= 1
            return True

        while l < r:
            if s[l] != s[r]:
                return isPalindrome(s, l, n) or isPalindrome(s, r, n)
            l += 1
            r -= 1
        return True


if __name__ == '__main__':
    str = 'asdfddsa'
    solution = Solution()
    print(solution.validPalindrome(str))
