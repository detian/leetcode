"""
7. 整数反转
中等
3.8K
相关企业
给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。

如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。

假设环境不允许存储 64 位整数（有符号或无符号）。


示例 1：

输入：x = 123
输出：321
示例 2：

输入：x = -123
输出：-321
示例 3：

输入：x = 120
输出：21
示例 4：

输入：x = 0
输出：0


提示：

-231 <= x <= 231 - 1



"""
from typing import List


class Solution:
    def reverse(self, x: int) -> int:
        if x >= 0:
            x = str(x)[::-1]
        elif x < 0:
            x = "-" + str(x).replace("-", "")[::-1]


        print( 2 ^ 31 - 1)
        print(int(x) < -2 ^ 31)
        if int(x) > 2 ^ 31 - 1 or int(x) < -2 ^ 31:
            return 0
        return int(x)


if __name__ == '__main__':
    x = 123
    solution = Solution()
    print(solution.reverse(x))
