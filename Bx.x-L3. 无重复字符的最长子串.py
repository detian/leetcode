from typing import List


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        current = ''
        maxs = ''
        temp = ''
        for i in s:
            # 判断 i 在不在max 中
            if current.find(i) != -1:

                if len(temp) < len(current):
                    temp = current
                current = current[current.find(i) + 1:]
                current += i
            else:
                current += i

            # 判断长度，然后替换
            if len(maxs) <= len(current):
                if (len(current) <= len(temp)):
                    maxs = temp
                else:
                    maxs = current

        return len(maxs)


if __name__ == '__main__':
    s = "gppsludzdtmccibi"
    solution = Solution()
    print(solution.lengthOfLongestSubstring(s))
