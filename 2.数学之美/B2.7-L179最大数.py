import functools
from typing import List


class Solution:
    def largestNumber(self, nums: List[int]) -> str:
        s = [str(i) for i in nums]

        def comp(a, b):
            if (a + b) > (b + a):
                return 1
            if (b + a) > (a + b):
                return -1
            return 0

        s.sort(reverse=True, key=functools.cmp_to_key(comp))
        return str(int("".join(s)))


if __name__ == '__main__':
    nums = [8308, 8308, 830]
    solution = Solution()
    print(solution.largestNumber(nums))
