from typing import List


# 分子 numerator 和分母 denominator
class Solution:
    def fractionToDecimal(self, numerator: int, denominator: int) -> str:
        # 先计算符号，再计算数值
        # divmod ==> a//b a%b
        n, remainder = divmod(abs(numerator), abs(denominator))
        signal = ''
        # 判断符号
        if numerator // denominator < 0:
            signal = '-'

        res = [str(n), '.']
        seen = []
        # 如果余数不在数组里，就把余数加入数组
        # 重新计算商和余数
        # 余数 * 10 除以分母

        while remainder not in seen:
            seen.append(remainder)
            n, remainder = divmod(remainder * 10, abs(denominator))
            res.append(str(n))

        # 处理循环节的格式
        index = seen.index(remainder)

        res.insert(index + 2, '(')
        res.append(')')
        # 删除末尾的 .
        return signal + ''.join(res).replace('(0)', '').rstrip('.')


if __name__ == '__main__':
    numerator = 1
    denominator = 2
    solution = Solution()
    print(solution.fractionToDecimal(numerator, denominator))
