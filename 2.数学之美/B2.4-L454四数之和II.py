# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from typing import List


class Solution:
    def fourSumCount(self, nums1: List[int], nums2: List[int], nums3: List[int], nums4: List[int]) -> int:
        mapper = {}
        res = 0
        for i in nums1:
            for j in nums2:
                mapper[i + j] = mapper.get(i + j, 0) + 1

        for k in nums3:
            for l in nums4:
                res += mapper.get(-1 * (k + l), 0)

        return res
        # Press the green button in the gutter to run the script.


if __name__ == '__main__':
    nums1 = [1, 2]
    nums2 = [-2, -1]
    nums3 = [-1, 2]
    nums4 = [0, 2]
    solution = Solution()
    print(solution.fourSumCount(nums1, nums2,nums3,nums4))

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
