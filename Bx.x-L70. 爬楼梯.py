from typing import List


class Solution:
    def climbStairs(self, n: int) -> int:
        return self.gogogo(n)

    def gogogo(self, n: int) -> int:
        if n == 0:
            return 0
        if n == 1:
            return 1
        if n == 2:
            return 2
        return self.gogogo(n - 1) + self.gogogo(n - 2)


if __name__ == '__main__':
    nums = [8308, 8308, 830]
    solution = Solution()
    print(solution.largestNumber(nums))
