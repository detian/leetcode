from typing import List


class Solution:
    def findMin(self, nums: List[int]) -> int:

        l, r = 0, len(nums) - 1
        while l <= r:

            mid = (l + r +1) // 2
            if l == r:
                return nums[l]
            elif nums[mid] > nums[r]:
                l = mid + 1
            elif nums[mid] < nums[r]:
                r = mid
        return -1


if __name__ == '__main__':
    nums = [2,3,1]
    solution = Solution()
    print(solution.findMin(nums))
