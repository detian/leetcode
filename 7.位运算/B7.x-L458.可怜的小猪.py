class Solution:
    def poorPigs(self, buckets: int, minutesToDie: int, minutesToTest: int) -> str:
        def numToXScale(x: int, buckets: int):
            res = ''
            while buckets != 0:
                res += str(buckets % x)
                buckets = buckets // x
            return res[::-1]

        return len(numToXScale(minutesToTest // minutesToDie + 1, buckets))


if __name__ == '__main__':
    buckets = 1000
    minutesToDie = 15
    minutesToTest = 60
    solution = Solution()
    print(solution.poorPigs(buckets, minutesToDie, minutesToTest))
