"""
请你判断一个 9 x 9 的数独是否有效。只需要 根据以下规则 ，验证已经填入的数字是否有效即可。

数字 1-9 在每一行只能出现一次。
数字 1-9 在每一列只能出现一次。
数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次。（请参考示例图）


注意：

一个有效的数独（部分已被填充）不一定是可解的。
只需要根据以上规则，验证已经填入的数字是否有效即可。
空白格用 '.' 表示。


示例 1：


输入：board =
[["5","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
输出：true
示例 2：

输入：board =
[["8","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
输出：false
解释：除了第一行的第一个数字从 5 改为 8 以外，空格内其他数字均与 示例1 相同。 但由于位于左上角的 3x3 宫内有两个 8 存在, 因此这个数独是无效的。


提示：

board.length == 9
board[i].length == 9
board[i][j] 是一位数字（1-9）或者 '.'
"""
from typing import List


class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:

        flag = [False] * 3

        # 获取每一列
        column_list = self.get_column(board[:])
        flag[1] = self.check(column_list)

        # 获取每一块

        block_list = self.get_block(board[:])
        flag[2] = self.check(block_list)

        # 获取每一行

        flag[0] = self.check(board[:])

        if flag.count(True) == 3:
            return True
        else:
            return False

    def get_column(self, all_list: List[List[str]]) -> List[List[str]]:
        ans = []

        for i in range(0, 9):
            temp_list = []
            for j in range(0, 9):
                temp_list.append((all_list[j])[i])
            ans.append(temp_list)
        return ans

    def get_block(self, all_list: List[List[str]]) -> List[List[str]]:
        ans: List[List[str]] = []

        # 横向拆分
        temp_rows = []
        temp_rows_list = []
        for i in range(0, 9):
            temp_rows.append(all_list[i])
            if len(temp_rows) == 3:
                temp_rows_list.append(temp_rows)
                temp_rows = []

        # 纵向拆分

        for rows in temp_rows_list:
            temp_row0 = []
            temp_row1 = []
            temp_row2 = []

            for i in range(0, 9):
                if i >= 0 and i < 3:
                    temp_row0.append((rows[0])[i])
                    temp_row0.append((rows[1])[i])
                    temp_row0.append((rows[2])[i])
                if i >= 3 and i < 6:
                    temp_row1.append((rows[0])[i])
                    temp_row1.append((rows[1])[i])
                    temp_row1.append((rows[2])[i])
                if i >= 6 and i < 9:
                    temp_row2.append((rows[0])[i])
                    temp_row2.append((rows[1])[i])
                    temp_row2.append((rows[2])[i])
            ans.append(temp_row0)
            ans.append(temp_row1)
            ans.append(temp_row2)
            # print(f'{temp_row0},{temp_row1},{temp_row2}')

        return ans

    # 判断行是否符合规则
    def check(self, rows: List[List[str]]) -> bool:
        for row in rows:
            # 清空 '.'
            count0 = row.count('.')
            for i in range(count0):
                row.remove('.')

            int_list = [int(x) for x in row]
            flag = True
            aaa = [0] * 9
            for i in int_list:
                aaa[i - 1] = int(aaa[i - 1]) + 1
                if aaa[i - 1] > 1:
                    return False
        return flag


if __name__ == '__main__':
    board = [["8","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
    solution = Solution()
    print(solution.isValidSudoku(board))
