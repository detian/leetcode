from typing import List
"""
6. N 字形变换
中等
2K
相关企业
将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。

比如输入字符串为 "PAYPALISHIRING" 行数为 3 时，排列如下：

P   A   H   N
A P L S I I G
Y   I   R
之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："PAHNAPLSIIGYIR"。

请你实现这个将字符串进行指定行数变换的函数：

string convert(string s, int numRows);
 

示例 1：

输入：s = "PAYPALISHIRING", numRows = 3
输出："PAHNAPLSIIGYIR"
示例 2：
输入：s = "PAYPALISHIRING", numRows = 4
输出："PINALSIGYAHRPI"
解释：
P     I    N
A   L S  I G
Y A   H R
P     I
示例 3：

输入：s = "A", numRows = 1
输出："A"
 

提示：

1 <= s.length <= 1000
s 由英文字母（小写和大写）、',' 和 '.' 组成
1 <= numRows <= 1000

"""

class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if len(s) == 1 or numRows == 1:
            return s
        strs = [""] * (numRows)
        zhouqi = numRows + numRows - 2
        column = numRows - 2;
        count = 1
        result = ''
        for i in s:
            yushu = count % zhouqi

            if yushu == 1:
                strs[0] += i
            elif yushu == numRows:
                strs[numRows - 1] += i
            elif yushu == 0:
                strs[1] += i
            else:
                for j in range(1, column+1):
                    if yushu == numRows + j or yushu == numRows - j:
                        strs[numRows - 1 - j] += i
                        # print(11)
            count += 1

        for i in range(numRows):
            result += strs[i]
        return result


if __name__ == '__main__':
    s = "PAYPALISHIRING"
    numRows = 4
    solution = Solution()
    print(solution.convert(s, numRows))
