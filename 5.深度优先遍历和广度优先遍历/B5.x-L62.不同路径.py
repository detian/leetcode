class Solution:
    def uniquePaths(self, m: int, n: int) -> int:

        self.ans = 0

        def dfs(r, c):
            # 结束条件
            if r == m - 1 and c == n - 1:
                self.ans += 1
                return
                # 向右 或者 向下移动
            if r < m:
                dfs(r + 1, c)
            if c < n:
                dfs(r, c + 1)

        dfs(0, 0)
        return self.ans