class Solution:
    def isPowerOfFour(self, n: int) -> bool:
        if n <= 0:
            return False
        if n == 1:
            return True

        a, b = n // 4, n % 4
        b = n - (n // 4) * 4
        while a >= 16:
            a = a // 4

        if (a == 1 or a == 4) and b == 0:
            return True
        else:
            return False


if __name__ == '__main__':
    nums = 20
    solution = Solution()
    print(solution.isPowerOfFour(nums))
