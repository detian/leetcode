from typing import List, Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        head = l3 = ListNode(0)
        temp = 0
        while l1 or l2:
            point = ListNode()

            temp = (l1.val + l2.val) / 10
            modNum = (l1.val + l2.val) % 10

            l1 = l1.next
            l2 = l2.next

            point.val = modNum
            l3.next = point
            l3 = l3.next

        return head.next


if __name__ == '__main__':
    l12 = ListNode(3, None)
    l11 = ListNode(4, l12)
    l1 = ListNode(2, l11)
    l22 = ListNode(3, None)
    l21 = ListNode(4, l22)

    l2 = ListNode(2, l21)

    solution = Solution()
    print(solution.addTwoNumbers(l1, l2))
